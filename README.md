# First shot 
node.js first server app with GET, POST, PUT, DElETE
 
If you want to use sample data you can rename /db/sample-data.json to fighters.json

## Installation

```sh
$ npm install
```

## Sample data 
If you want to use sample data in the App, run:
```sh
$ npm run load-sample-data
```
***Note!*** Your existing data will be overwritten 

## Usage
Run this command in terminal:
```sh
$ nodemon start
```
Open http://localhost:3000/
You have a couple of options here:

Get an array of all users (GET: /user)

Receive data of one particular user (GET: /user/:id)

Create a new user (POST: /user)

Update existing user (PUT: /user/:id)

Delete existing user (DELETE: /user/:id)

***Note!*** To be able to change or delete data you need to send key:value Authorization:admin pair in the header's request.

## License

[MIT](LICENSE)
