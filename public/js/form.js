/* public/js/form.js */

(function () {
    let form = document.getElementById('edit-form');
    form.onsubmit = (event) => {
        let isValid = true;
        const data = {};
        const elements = form.elements;
        for (let i = 0; i < elements.length; i++) {
            if (!elements[i].matches('input')) {
                continue;
            } else if (!elements[i].matches(':valid')) {
                isValid = false;
                break;
            }
            data[elements[i].name] = elements[i].value;
        }
        if (isValid){
            event.preventDefault();
            fetch(form.action, {
                method: form.getAttribute('x-method'),
                headers: new Headers({
                    'content-type': 'application/json',
                    'authorization': 'admin'
                }),
                body: JSON.stringify(data)
            })
                .then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(data => {
                    if (data.location) {
                        location.assign(data.location);
                    }
                })
        }
    };
    let deleteButton = document.getElementById('delete-button');
    if (deleteButton) {
        deleteButton.onclick = function (event) {
            event.preventDefault();
            fetch(form.action, {
                method: 'DELETE',
                headers: new Headers({
                    'authorization': 'admin'
                })
            })
                .then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(data => {
                    if (data.location) {
                        location.assign(data.location);
                    }
                })
        }
    }
})();
