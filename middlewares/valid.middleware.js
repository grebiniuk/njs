/* middlewares/valid.middleware.js */

let isValidUserData = function(req, res, next) {
  let {name, health, attack, defense, source} = req.body;
  if (
    !(name && typeof name === 'string' &&
      health && (typeof health === 'string' && health.match(/\d+/) || Number.isInteger(health)) &&
      attack && (typeof attack === 'string' && attack.match(/\d+/) || Number.isInteger(attack)) &&
      defense && (typeof defense === 'string' && defense.match(/\d+/) || Number.isInteger(defense)) &&
      source && typeof source === 'string')
  ){
    res.status(400).json({message: "Bad Request"});
  } else {
    next();
  }
};

module.exports = {
  isValidUserData
};
