/* routes/users.js */

let express = require('express');
let router = express.Router();

const { fetchUsers, getUser, saveUsers } = require('../services/user.service');
const { isAuthorized } = require('../middlewares/auth.middleware');
const { isValidUserData } = require('../middlewares/valid.middleware');

router.use(fetchUsers);
router.use(saveUsers);

router.get('/', function(req, res) {
  res.json(req.users.data());
});

router.get('/:id([0-9]+)', getUser, function(req, res){
  res.json(req.user);
});

router.get('/create', function(req, res){
  res.render('form', {
    user: {},
    action: '/user/',
    method: 'POST'
  });
});

router.post('/', isAuthorized, isValidUserData, function(req, res) {
  let user = req.users.create(req.body);
  if (user) {
    res.json({
      message: `Fighter with id ${user._id} is created.`,
      location: `/user/${user._id}`
    });
  } else {
    res.status(400).json({message: "Bad Request"});
  }
});

router.get('/update/:id([0-9]+)', getUser, function(req, res){
  res.render('form', {
    user: req.user,
    action: `/user/${req.user._id}`,
    method: 'PUT'
  });
});

router.put('/:id([0-9]+)', isAuthorized, getUser, isValidUserData, function(req, res){
  console.log('req.body', req.body);
  let user = req.users.update(req.user._id, req.body);

  if (user) {
    res.json({
      message: `Fighter with id ${user._id} is updated.`,
      location: `/user/${user._id}`
    });
  } else {
    res.status(400).json({message: "Bad Request"});
  }
});

router.delete('/:id([0-9]+)', isAuthorized, getUser, function(req, res){
  if (req.users.drop(req.user._id))  {
    res.json({
      message: `Fighter with id ${req.user._id} is deleted.`,
      location: '/user'
    });
  } else {
    res.status(400).json({message: "Bad Request"});
  }
});

module.exports = router;
