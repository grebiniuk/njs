let express = require('express');
let cookieParser = require('cookie-parser');
let logger = require('morgan');

let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');
let app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/static', express.static('public'));
app.set('view engine', 'pug');
app.set('views', './views');

app.use('/', indexRouter);
app.use('/user', usersRouter);

app.get('*', function(req, res){
    res.status(404).send('Sorry, this is an invalid URL.');
});


module.exports = app;
