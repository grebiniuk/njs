/* services/user.service.js */

const fs = require('fs');

const __users__ = 'db/users.json';

class Users {
  constructor(data) {
    this._users = JSON.parse(data);
    this._changed = false;
  }

  isChanged() {
    return this._changed;
  }

  stringify() {
    return JSON.stringify(this._users);
  }

  data() {
    return this._users.reduce((data, user) => {
      data.push(Object.assign({}, user));
      return data;
    }, []);
  }

  create({name, health, attack, defense, source}) {
    let _id = String(this._users.reduce((n, user) => Math.max(n, Number(user._id)), 0) + 1);
    [health, attack, defense] = [health, attack, defense].map(value => Number(value));
    this._users.push({_id, name, health, attack, defense, source});
    this._changed = true;
    return this.read(_id);
  }

  read(id) {
    let user = this._users.find(user => user._id === id);
    return user ? Object.assign({}, user) : null;
  }

  update(id, {name, health, attack, defense, source}) {
    let user = this._users.find(user => user._id === id);
    [health, attack, defense] = [health, attack, defense].map(value => Number(value));
    if (user) {
      Object.assign(user, {name, health, attack, defense, source});
      this._changed = true;
      return this.read(id);
    }
  }

  drop(id) {
    let index = this._users.findIndex(user => user._id === id);
    console.log(index);
    if (index >= 0) {
      this._users.splice(index, 1);
      return this._changed = true;
    }
  }
}

function fetchUsers(req, res, next) {
  fs.readFile(__users__, 'utf8', (err, data) => {
    if (err) {
      if (err.code !== 'ENOENT') {
        console.error(err);
        res.status(500).send('Internal Server Error');
        return;
      }
      data = '[]';
    }
    req.users = new Users(data);
    next();
  });
}

function saveUsers(req, res, next) {
  res.on('finish', function () {
    if (!req.users.isChanged()) {
      return;
    }
    fs.writeFile(__users__, req.users.stringify(), 'utf8', (err) => {
      if (err) {
        console.error(err);
      }
    });
  });
  next();
}

function getUser(req, res, next) {
  req.user = req.users.read(req.params.id);
  if (!req.user) {
    res.status(404).json({
      message: `Fighter with id ${req.params.id} was not found.`
    });
  } else {
    next();
  }
}


module.exports = {
  fetchUsers,
  saveUsers,
  getUser
};
